﻿namespace Reflection
{
    public class Settings
    {
        [ConfigurationItem(nameof(Name), ProviderType.ConfigurationManager)]
        public string Name { get; set; }

        [ConfigurationItem(nameof(Age), ProviderType.ConfigurationManager)]
        public int Age { get; set; }

        [ConfigurationItem(nameof(Height), ProviderType.ConfigurationManager)]
        public float Height { get; set; }

        [ConfigurationItem(nameof(Days), ProviderType.ConfigurationManager)]
        public TimeSpan Days { get; set; }
    }
}
