﻿namespace Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var settings = new Settings();
            
            settings.Name = "Alex";
            settings.Age = 23;
            settings.Height = (float)189.5;
            settings.Days = TimeSpan.FromDays(8395);

            ConfigurationComponentBase.SaveSettings(settings);
            ConfigurationComponentBase.LoadSettings();
        }
    }
}

