﻿using Newtonsoft.Json;
using System.Reflection;

namespace Reflection
{
    public class ConfigurationManagerConfigurationProvider
    {
        private readonly string path;

        public ConfigurationManagerConfigurationProvider()
        {
            path = @"C:\Users\Oleksandr_Krashanov\source\repos\Reflection\Reflection\Files\appsetting.json";
        }

        public void LoadSettings(Settings settings)
        {
            var textFromAppSettingFile = File.ReadAllText(path);

            var readSettings = JsonConvert.DeserializeObject<Settings>(textFromAppSettingFile);

            settings.Name = readSettings.Name;
            settings.Age = readSettings.Age;
            settings.Height = readSettings.Height;
            settings.Days = readSettings.Days;
        }

        public void SaveSettings(IEnumerable<PropertyInfo> properties, Settings settings)
        {
            string text = "{\n";
            foreach (var property in properties)
            {
                text += $"\t\"{property.Name}\": \"{property.GetValue(settings)}\",\n";
            }
            text += "}";
            File.WriteAllText(path, text);
        }
    }
}
