﻿using System.Reflection;

namespace Reflection
{
    public class ConfigurationComponentBase
    {
        public static void SaveSettings(Settings settings)
        {
            var type = typeof(Settings);
            var appSettingProvider = new ConfigurationManagerConfigurationProvider();
            var propertiesForAppSetting = new List<PropertyInfo>();

            foreach (var property in type.GetProperties())
            {
                propertiesForAppSetting.Add(property);
            }

            appSettingProvider.SaveSettings(propertiesForAppSetting, settings);
        }

        public static Settings LoadSettings()
        {
            var settings = new Settings();
            var appSettingProvider = new ConfigurationManagerConfigurationProvider();

            appSettingProvider.LoadSettings(settings);

            return settings;
        }
    }
}
